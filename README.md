# PROYECT BRIEF:
	1.Project summary: This project contains the code of titoma training task in which you can find some
	functions that work with timers and interruptions to interact with the leds of the curiosity microchip board 
	PIC32MX470F512H and an AD conversion to read a value from a potentiometer and UART communication.
	
	Some of the functionality are the next ones:
	
	1. Toogle Led2 each second: uses core timer to control the time to toggle the led.
	2. Send uart tick message: uses core timer to control how often tick message is sent via uart.
	3. Toggle led1 switch pressed: uses core timer to control the flags that indicate when the led start to toggle according to switch state.
	4. Stop and restart led blinking: uses core timer to control de counter that change the state of the led(blinking or off).	
	5. PWM manual: uses core timer to restart the values of the variables that control the times. 
	6. Toogle Led1 each second: uses timer 1 to control the time to toggle the led.
	7. ADC conversion uart: uses timer 1 to control how often a lecture from ADC is sent via uart.
	8. PWM blue rgb: uses timer 1 to restart the values of the variables that control the times and uart reception to receive the values of
		the variables that are being sent from command.
	9. PWM green rgb: uses timer 1 to restart the values of the variables that control de times and uart reception to receive the values of
		the variables that are being sent from cmd.
	10. Uart message reception: receive a command from console and set the value to another variable to control PWM of green and blue RGB led.
		The commands that need to be send from console are:
# Commands
		-> GADC : Get ADC value
		-> GMAO value : Set Green LED Macro ON time
			->GMAO XXXX
		-> GMAP value : Set Green LED Macro time
			->GMAp XXXX
		-> GMIP value : Set Green LED Micro ON time
			->GMIO XXXX
		-> GMIP value : Set Green LED Micro time
			->GMIP XXXX
		-> BMAO value : Set Blue LED Macro ON  time
			->BMAO XXXX
		-> BMAP value : Set Blue LED Macro time
			->BMAP XXXX
		-> BMIO value : Set Blue LED Micro ON time
			->BMIO XXXX
		-> BMIP value : Set Blue LED Micro time
			->BMIP XXXX
		-> GENB value : Enable / disable green LED blink
			-> To Enable set 1 as value. / To disable set 0 as value.
		-> BENB value : Enable / disable Blue LED blink
		
			-> To Enable set 1 as value. / To disable set 0 as value.
			-> value could be a number between 0 and 65535.
			-> Macro time must be greater than Macro ON time.
			-> Micro time must be greater than Micro ON time.	

# HW DESCRIPTION:
	1. Links to MCU´s datasheet:
	-PIC32MX470F512H datasheet
	 http://ww1.microchip.com/downloads/en/devicedoc/60001185g.pdf
	-Curiosity Board user guide
	 http://ww1.microchip.com/downloads/en/DeviceDoc/70005283B.pdf

# SERIAL COMMANDS:
	1 Link to the commands source/header.
		source --- main.c 
		https://bitbucket.org/EstebanPachon/ttm00-fw-curiosity_4-pic32/src/develop/TTM00%20-%20%20FW%20-%20Curiosity_4%20-%20PIC32.X/Src/
		header -- main.h 
		https://bitbucket.org/EstebanPachon/ttm00-fw-curiosity_4-pic32/src/develop/TTM00%20-%20%20FW%20-%20Curiosity_4%20-%20PIC32.X/Inc/
		
	2 Serial command file for serial terminal:
		docklight
		https://docklight.de/downloads/
		Coolterm
		

	3 Serial configuration:
		-Baud Rate  --- 115200
		-Parity --- None	
		-Parity Error Char. --- (ignore)
		-Data Bits ---- 8
		-Stop Bits --- 1
		-Send/Receive Com --- The one assigned by your PC.

# PRERREQUISITES:

	1 SDK version: MPLAB V3.61

	2 IDE : MPLAB X IDE V3.61

	3 Compiler version : XC 32 V1.42

	4 Project configuration:
		Categories : Microchip Embedded 
		Projects : Standalone Project
		Family : All Families
		Device : PIC32MX470F512H
		Hardware Tools : Microchip starter kits
		Compiler : XC32 (v1.42)
		Encoding: ISO-8859-1
		
# VERSIONING: 

	1 Current version of the fw/sw  V2.1.20190803

	2 Description of the last version changes: 
		-Constant variables was changed for structures
		-New routines were created. 
		-New comments were added to explain how the routines work.
		
# AUTHORS: 	

	1 Project staff : Esteban Pachón

	2 Maintainer contact : 
		-email: esteban.pachon@titoma.com
		-Bitbucket user : EstebanPachon


