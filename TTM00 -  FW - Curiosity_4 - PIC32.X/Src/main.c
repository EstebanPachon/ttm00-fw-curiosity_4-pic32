
/** @file        main.c
    * @author    EstebanPachon
    * @version   V1.1.20191026
    * @date      26-Oct-2019, 19:13 PM
    * @brief     Titoma training task
 
    \mainpage Description
 This code uses CORETIMER, TIMER1 and CHANGE NOTICE VECTOR interruptions to turn on,
 turn off or toggle LEDs from curiosity board according to some parameters of time that
 are sent via UART communication or define by the user. 

 */
#include "main.h"

void main(void){   

    main_setup();   
  
    while(1)
    {   //toggle LED_2 each second
        toggle_LED2_each_second();

        // Send via UART TICK each 1.5 second
        send_UART_tick_message();
            
        //Led_1 is toggle if SW1 is pressed at least 0.5 second
        toggle_LED_switch_pressed();    
            
        //functionality to stop LED2 blinking if SW1 is held for 2000mS or more.
        //It should re-start blinking if SW1 is again held for 2000mS or more.
        stop_and_restart_LED_blinking();
            
        //Manual PWM
        pwm_manual_potentiometer();
            
        //Led_1 is toggle each second
        toggle_LED1_each_second();
        
        // takes a value from ADC and sent it via UART serial communication
        sent_adc_conversion_uart();
        
        // change the pwm values of led rgb blue with parameters sent from console       
        control_pwm_blue_rgb_via_uart();
        
        // change the pwm values of led rgb green with parameters sent from console
        control_pwm_green_rgb_via_uart();
        
        //select the control parameter that define the functionality to do
        uart_message_reception();
    }//while(1)        
}//void main(void)

void main_setup(void){
    adc_init_configuration();
    init_configurations();
    load_initial_values();
    pwm_rgb_led_and_uart_initial_values();
    timers_initialiaze_configuration();
    start_uart_communication();
}

/** 
 * @brief Define general and initial configuration parameters
*/
void init_configurations(void){
    
    //PRINT_MESSAGE_SIMPLE("Booting " FW_VER "-" AUTHOR);           
      
    //IO pins configuration    
    mPORTBSetPinsDigitalOut(PINS_CREE);
    mPORTBSetBits(PINS_CREE);
    mPORTESetPinsDigitalOut(PINS_LEDS);
    mPORTEClearBits(PINS_LEDS);
    mPORTDSetPinsDigitalIn(PIN_SW);
    mPORTBSetPinsAnalogIn(BIT_14); //RB14 -> AN14 channel
}

/** 
 * @brief Define general and initial values of variables
*/
void load_initial_values(void){
    pwm_led_configuration.LED_pwm_on = 1;
    pwm_led_configuration.LED_pwm_period = 20;
    pwm_led_configuration.macro_time_on = 1000;
    pwm_led_configuration.macro_time_period = 2000;
    
    variable_seconds_configuration.LED_millis = 1000;
    variable_seconds_configuration.LED1_millis = 1000;
    variable_seconds_configuration.UART_millis = 1500;
    variable_seconds_configuration.ADC_lecture_millis = 1500;
    
    aux_variable_configuration.led_counter = FALSE;
    aux_variable_configuration.button_push = FALSE;
    aux_variable_configuration.switch_millis_counter = FALSE;
    aux_variable_configuration.switch_state_LED_blinking = FALSE;
    aux_variable_configuration.flag_LED_stop_blinking = FALSE;
}

/** 
 * @brief Define general and initial values of RGB led and UART communication
*/
void pwm_rgb_led_and_uart_initial_values(void){
    led_green_rgb.pin = BIT_3;
    led_green_rgb.port = 'B';
    led_green_rgb.led_status = FALSE;
    
    led_blue_rgb.pin = BIT_2;
    led_blue_rgb.port = 'B';
    led_blue_rgb.led_status = FALSE;
    
    uart_value_green_rgb.UART_value_macro_on_time = FALSE;
    uart_value_green_rgb.UART_value_macro_period_time= FALSE;
    uart_value_green_rgb.UART_value_micro_on_time = FALSE;
    uart_value_green_rgb.UART_value_micro_period_time = FALSE;
    
    uart_value_blue_rgb.UART_value_macro_on_time = FALSE;
    uart_value_blue_rgb.UART_value_macro_period_time= FALSE;
    uart_value_blue_rgb.UART_value_micro_on_time = FALSE;
    uart_value_blue_rgb.UART_value_micro_period_time = FALSE;
}

/** 
 * @brief Define general timers parameters
*/
void timers_initialiaze_configuration(void){
    //CoreTimer configuration
    OpenCoreTimer(CORE_TICK_RATE);
    mConfigIntCoreTimer(CT_INT_ON | CT_INT_PRIOR_3 | CT_INT_SUB_PRIOR_0);
    INTEnableSystemMultiVectoredInt(); 
    
    //Timer1 configuration
    OpenTimer1((T1_ON | T1_SOURCE_INT | T1_PS_1_8), T1_TICK_RATE);
    ConfigIntTimer1(T1_INT_ON | T1_INT_PRIOR_2 | T1_INT_SUB_PRIOR_3);
    INTEnableSystemMultiVectoredInt();
    
    //Change Notice Interrupt configuration
    mCNDOpen(CONFIGURATION, PINSWITCH, PULLUPS);
    ConfigIntCND(INTERRUPT);
}

/** 
 * @brief Define ADC initial configuration parameters
*/
void adc_init_configuration(void){      
    //ADC CONFIGURATION
    // ensure the ADC is off before setting the configuration
    CloseADC10();
    // configure to sample AN14
    SetChanADC10(PARAM);
    // configure ADC and enable it
    OpenADC10(PARAM1, PARAM2, PARAM3, PARAM4, PARAM5);
    // Now enable the ADC logic
    EnableADC10();
}

void __ISR(_CORE_TIMER_VECTOR,IPL3AUTO) CoreTimerHandler(void){
    mCTClearIntFlag();
    UpdateCoreTimer(CORE_TICK_RATE);
    core_timer_counter_verification_values();
    
    //toggle LED_1 each 0.5 second with SW1
    if(!PORTDbits.RD6){
        if(aux_variable_configuration.button_push == TRUE){}
        else aux_variable_configuration.led_counter++;
    }
    else{
        aux_variable_configuration.button_push = FALSE;
    }
        
    //functionality to stop LED2 blinking if SW1 is held for 2000mS or more
    //It should re-start blinking if SW1 is again held for 2000mS or more.
    if(!PORTDbits.RD6){
        if(aux_variable_configuration.switch_state_LED_blinking == TRUE){}
        else aux_variable_configuration.switch_millis_counter++;
    }      
    else{    
        aux_variable_configuration.switch_state_LED_blinking = FALSE;
    }
}

void __ISR(_TIMER_1_VECTOR, IPL2AUTO) Timer1Handler(void){
    mT1ClearIntFlag();
    //update the period
    WritePeriod1(T1_TICK_RATE); 
    //toggle led_1 each second
    if(variable_seconds_configuration.LED1_millis)
        variable_seconds_configuration.LED1_millis--; 
    
    timer1_counter_pwm_rgb_verification_values();
}

void __ISR(_CHANGE_NOTICE_VECTOR, IPL6AUTO) ChangeNoticeHandler(void){
    mCNDClearIntFlag();
    mPORTDRead();
    mPORTBToggleBits(PINS_CREE);
}

/** 
 * @brief control and decrement core timer functions parameters
*/
void core_timer_counter_verification_values(void){
    //LED 2 is toggle each second
    if(variable_seconds_configuration.LED_millis)
        variable_seconds_configuration.LED_millis--;
    
    // Send via UART TICK each 1.5 second
    if(variable_seconds_configuration.UART_millis)
        variable_seconds_configuration.UART_millis--;
    
    // Send via UART TICK each 1.5 second
    if(variable_seconds_configuration.ADC_lecture_millis)
        variable_seconds_configuration.ADC_lecture_millis--;
    
    //Manual PWM
    if(pwm_led_configuration.macro_time_on)
        pwm_led_configuration.macro_time_on--;
    if(pwm_led_configuration.macro_time_period)
        pwm_led_configuration.macro_time_period--;
    if(pwm_led_configuration.LED_pwm_period)
        pwm_led_configuration.LED_pwm_period--;
    if(pwm_led_configuration.LED_pwm_on)
        pwm_led_configuration.LED_pwm_on--;
}
    
/** 
 * @brief control and decrement timer 1 functions parameters
*/
void timer1_counter_pwm_rgb_verification_values(void){
    pwm_rgb_red_led_verification_value();
    pwm_rgb_green_led_verification_value();
    pwm_rgb_blue_led_verification_value();        
}

void pwm_rgb_red_led_verification_value(){
    if (led_red_rgb.led_status == TRUE){
        if (led_red_rgb.macro_on_rgb_time)
            led_red_rgb.macro_on_rgb_time--;
        if (led_red_rgb.macro_period_rgb_time)
            led_red_rgb.macro_period_rgb_time--;
        if (led_red_rgb.micro_on_rgb_time)
            led_red_rgb.micro_on_rgb_time--;
        if (led_red_rgb.micro_period_rgb_time)
            led_red_rgb.micro_period_rgb_time--;
    }    
}

void pwm_rgb_green_led_verification_value(){
    if (led_green_rgb.led_status == TRUE){
        if (led_green_rgb.macro_on_rgb_time)
            led_green_rgb.macro_on_rgb_time--;
        if (led_green_rgb.macro_period_rgb_time)
            led_green_rgb.macro_period_rgb_time--;
        if (led_green_rgb.micro_on_rgb_time)
            led_green_rgb.micro_on_rgb_time--;
        if (led_green_rgb.micro_period_rgb_time)
            led_green_rgb.micro_period_rgb_time--;
    }    
}

void pwm_rgb_blue_led_verification_value(){
    if (led_blue_rgb.led_status == TRUE){
        if (led_blue_rgb.macro_on_rgb_time)
            led_blue_rgb.macro_on_rgb_time--;
        if (led_blue_rgb.macro_period_rgb_time)
            led_blue_rgb.macro_period_rgb_time--;
        if (led_blue_rgb.micro_on_rgb_time)
            led_blue_rgb.micro_on_rgb_time--;
        if (led_blue_rgb.micro_period_rgb_time)
            led_blue_rgb.micro_period_rgb_time--;
    }    
}

/** 
 * @brief Start UART communication parameters
*/
void start_uart_communication(void){
    UART1_init(115200);
    UART2_init(115200);
    UART1_write_string("[PIC]: Initializing...\r\n");
    UART2_write_string("[PIC]: Initializing...\r\n");
   }

/** 
 * @brief change the state of led1 each second
*/
void toggle_LED1_each_second(void){
    if(!variable_seconds_configuration.LED1_millis){
        variable_seconds_configuration.LED1_millis = 1000;
        LED_toggle(e_LED_1);
    }
}

/** 
 * @brief change the state of led2 each second
*/
void toggle_LED2_each_second(void){
    if(!variable_seconds_configuration.LED_millis && (aux_variable_configuration.flag_LED_stop_blinking == FALSE)){
        variable_seconds_configuration.LED_millis = 1000; 
        LED_toggle(e_LED_2);
        } 
}

/** 
 * @brief Send a message via UART communication
*/
void send_UART_tick_message(void){
    if(!variable_seconds_configuration.UART_millis){
        variable_seconds_configuration.UART_millis = 1500;
        UART1_write_string("Tick\r\n");
        UART2_write_string("Debug\r\n");
    }
}

/** 
 * @brief check if Switch was pressed and toggle a led.
*/
void toggle_LED_switch_pressed(void){
    if(aux_variable_configuration.led_counter >=500 && aux_variable_configuration.button_push == FALSE){
        LED_toggle(e_LED_1);
        aux_variable_configuration.button_push = TRUE;
        aux_variable_configuration.led_counter = FALSE;
    }
}

/** 
 * @brief check state of led and change it depending on the switch.
*/
void stop_and_restart_LED_blinking(void){
    if(aux_variable_configuration.switch_millis_counter >= 2000 && aux_variable_configuration.switch_state_LED_blinking == FALSE){
        if(aux_variable_configuration.flag_LED_stop_blinking == FALSE){
            LED_off(e_LED_2);
            aux_variable_configuration.switch_state_LED_blinking = TRUE;
            aux_variable_configuration.switch_millis_counter = FALSE;
            aux_variable_configuration.flag_LED_stop_blinking = TRUE;
            }
        else{
            aux_variable_configuration.flag_LED_stop_blinking = 0;
            aux_variable_configuration.switch_state_LED_blinking = 1;
            aux_variable_configuration.switch_millis_counter = 0;
            }
    }
}

/** 
 * @brief ADC conversion and sent values via UART communication
*/
void sent_adc_conversion_uart(void){
    if(!variable_seconds_configuration.ADC_lecture_millis){
        variable_seconds_configuration.ADC_lecture_millis = 1500;
        
        // determine which buffer is idle and create an offset
        ADC_offset = 8 * ((~ReadActiveBufferADC10() & 0x01));
        
        // read the result of channel 14 conversion in the idle buffer
        ADC_channel14_value = ReadADC10(ADC_offset);
        //UART communication
        
        PRINT_MESSAGE("ADC value is : %d\n",ADC_channel14_value);
    }
}

/** 
 * @brief Control PWM to turn a Led.
*/
void pwm_manual_potentiometer(void){
    if (!pwm_led_configuration.LED_pwm_on){
        LED_off(e_LED_3);
    }
    else{
        LED_on(e_LED_3);
    }
    
    if ((!pwm_led_configuration.LED_pwm_period) && (pwm_led_configuration.macro_time_on)){
        pwm_led_configuration.LED_pwm_period = 20;
        pwm_led_configuration.LED_pwm_on =  (ADC_channel14_value*pwm_led_configuration.LED_pwm_period)/1023; 
    }
    
    if (!pwm_led_configuration.macro_time_period){
        pwm_led_configuration.LED_pwm_period = 20;
        pwm_led_configuration.LED_pwm_on = (ADC_channel14_value*pwm_led_configuration.LED_pwm_period)/1023;
        pwm_led_configuration.macro_time_on = 1000;
        pwm_led_configuration.macro_time_period = 2000;
    }
}

/** 
 * @brief Control PWM to turn RGB blue Led.
*/
void control_pwm_blue_rgb_via_uart(void){
  if (led_blue_rgb.led_status == TRUE){
    if (!led_blue_rgb.micro_on_rgb_time)  
      LED_off (LED_B);
    else 
      LED_on (LED_B);     
    if ((!led_blue_rgb.micro_period_rgb_time) && (led_blue_rgb.macro_on_rgb_time)){
      led_blue_rgb.micro_period_rgb_time = uart_value_blue_rgb.UART_value_micro_period_time;
      led_blue_rgb.micro_on_rgb_time = uart_value_blue_rgb.UART_value_micro_on_time;
    }    
    if (!led_blue_rgb.macro_period_rgb_time){
      led_blue_rgb.macro_period_rgb_time = uart_value_blue_rgb.UART_value_macro_period_time;
      led_blue_rgb.macro_on_rgb_time = uart_value_blue_rgb.UART_value_macro_on_time;
      led_blue_rgb.micro_period_rgb_time = uart_value_blue_rgb.UART_value_micro_period_time;
      led_blue_rgb.micro_on_rgb_time = uart_value_blue_rgb.UART_value_micro_on_time;
    }
  }
}

/** 
 * @brief Control PWM to turn RGB green Led.
*/
void control_pwm_green_rgb_via_uart(void){
  if (led_green_rgb.led_status == TRUE){
    if (!led_green_rgb.micro_on_rgb_time)  
      LED_off (LED_G);
    else 
      LED_on (LED_G);    
    if ( (!led_green_rgb.micro_period_rgb_time) && (led_green_rgb.macro_on_rgb_time)){
      led_green_rgb.micro_period_rgb_time = uart_value_green_rgb.UART_value_micro_period_time;
      led_green_rgb.micro_on_rgb_time= uart_value_green_rgb.UART_value_micro_on_time;
    }    
    if (!led_green_rgb.macro_period_rgb_time){
      led_green_rgb.macro_period_rgb_time = uart_value_green_rgb.UART_value_macro_period_time;
      led_green_rgb.macro_on_rgb_time = uart_value_green_rgb.UART_value_macro_on_time;
      led_green_rgb.micro_period_rgb_time = uart_value_green_rgb.UART_value_micro_period_time;
      led_green_rgb.micro_on_rgb_time = uart_value_green_rgb.UART_value_micro_on_time;
    }
  }
}

/** 
 * @brief Control PWM to turn RGB LED.
*/
void uart_message_reception(void){
    if (UART1_is_unread()){
        UART1_clear_unread();
        sscanf(U1RxBuf,"%s %hu",command_sent_from_console,&value_int);  
        uint16_t uart_command_selected = uart_compare_to_select_command(command_sent_from_console);
        switch (uart_command_selected){
            case e_get_adc_value:
                PRINT_MESSAGE ("ADC Value Lecture is: %d\n",ADC_channel14_value); 
                break;
            case e_green_macro_on:
                uart_value_green_rgb.UART_value_macro_on_time = value_int;
                led_green_rgb.macro_on_rgb_time = uart_value_green_rgb.UART_value_macro_on_time;
                break;
            case e_green_macro_period:
                uart_value_green_rgb.UART_value_macro_period_time = value_int;
                led_green_rgb.macro_period_rgb_time = uart_value_green_rgb.UART_value_macro_period_time;
                break;
            case e_green_micro_on:
                uart_value_green_rgb.UART_value_micro_on_time = value_int;
                led_green_rgb.micro_on_rgb_time = uart_value_green_rgb.UART_value_micro_on_time;
                break;
            case e_green_micro_period:
                uart_value_green_rgb.UART_value_micro_period_time = value_int;
                led_green_rgb.micro_period_rgb_time = uart_value_green_rgb.UART_value_micro_period_time;
                break;
            case e_blue_macro_on:
                uart_value_blue_rgb.UART_value_macro_on_time = value_int;
                led_blue_rgb.macro_on_rgb_time = uart_value_blue_rgb.UART_value_macro_on_time;
              break;
            case e_blue_macro_period:
                uart_value_blue_rgb.UART_value_macro_period_time = value_int;
                led_blue_rgb.macro_period_rgb_time = uart_value_blue_rgb.UART_value_macro_period_time;
              break;
            case e_blue_micro_on:
                uart_value_blue_rgb.UART_value_micro_on_time = value_int;
                led_blue_rgb.micro_on_rgb_time = uart_value_blue_rgb.UART_value_micro_on_time;
              break;
            case e_blue_micro_period:
                uart_value_blue_rgb.UART_value_micro_period_time = value_int;
                led_blue_rgb.micro_period_rgb_time = uart_value_blue_rgb.UART_value_micro_period_time;
              break;
            case e_green_enable_blink:
                if (value_int == 1) led_green_rgb.led_status = TRUE;
                if (value_int == 0) led_green_rgb.led_status = FALSE;
                break;
            case e_blue_enable_blink:
                if(value_int == 1) led_blue_rgb.led_status = TRUE;
                if(value_int == 0) led_blue_rgb.led_status = FALSE;
                break;
            default:
                break;
        }//switch (uart_value_selected)
  }//if (UART1_is_unread())
}//void uart_message_reception(void)

/** 
 * @brief Control PWM to turn RGB blue Led.
 * @param uin8_t pointer to a variable.
 * @retval <ret> value from 1 to 11 that correspond to a specific condition, 0 otherwise. 
*/
uint8_t uart_compare_to_select_command(uint8_t *command_sent_from_console){
  if (!memcmp(command_sent_from_console,"GADC",4)) return e_get_adc_value;
  if (!memcmp(command_sent_from_console,"GMAO",4)) return e_green_macro_on;
  if (!memcmp(command_sent_from_console,"GMAP",4)) return e_green_macro_period;
  if (!memcmp(command_sent_from_console,"GMIO",4)) return e_green_micro_on;
  if (!memcmp(command_sent_from_console,"GMIP",4)) return e_green_micro_period;
  if (!memcmp(command_sent_from_console,"BMAO",4)) return e_blue_macro_on;
  if (!memcmp(command_sent_from_console,"BMAP",4)) return e_blue_macro_period;
  if (!memcmp(command_sent_from_console,"BMIO",4)) return e_blue_micro_on;
  if (!memcmp(command_sent_from_console,"BMIP",4)) return e_blue_micro_period;
  if (!memcmp(command_sent_from_console,"GENB",4)) return e_green_enable_blink;
  if (!memcmp(command_sent_from_console,"BENB",4)) return e_blue_enable_blink;
  else 
    return 0;
}
