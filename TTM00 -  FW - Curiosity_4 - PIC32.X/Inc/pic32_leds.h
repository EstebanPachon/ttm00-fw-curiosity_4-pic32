#include <stdio.h>
#include <stdlib.h>
#include<stdint.h>
#include <plib.h>

enum{
    e_LED_1,
    e_LED_2,
    e_LED_3,
    LED_R,
    LED_G,
    LED_B
};

void LED_off(uint8_t LED);
void LED_on(uint8_t LED);
void LED_toggle(uint8_t LED);
void error_handler();

