// PIC32MX470F512H Configuration Bit Settings

// 'C' source line config statements

// DEVCFG3
// USERID = No Setting
#pragma config FSRSSEL = PRIORITY_7     // Shadow Register Set Priority Select (SRS Priority 7)
#pragma config PMDL1WAY = OFF           // Peripheral Module Disable Configuration (Allow multiple reconfigurations)
#pragma config IOL1WAY = OFF            // Peripheral Pin Select Configuration (Allow multiple reconfigurations)
#pragma config FUSBIDIO = ON            // USB USID Selection (Controlled by the USB Module)
#pragma config FVBUSONIO = ON           // USB VBUS ON Selection (Controlled by USB Module)

// DEVCFG2
#pragma config FPLLIDIV = DIV_5         // PLL Input Divider (5x Divider)
#pragma config FPLLMUL = MUL_24         // PLL Multiplier (24x Multiplier)
#pragma config UPLLIDIV = DIV_12        // USB PLL Input Divider (12x Divider)
#pragma config UPLLEN = OFF             // USB PLL Enable (Disabled and Bypassed)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)

// DEVCFG1
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = HS             // Primary Oscillator Configuration (HS osc mode)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_1           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/1)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1048576        // Watchdog Timer Postscaler (1:1048576)
#pragma config WINDIS = OFF             // Watchdog Timer Window Enable (Watchdog Timer is in Non-Window Mode)
#pragma config FWDTEN = OFF             // Watchdog Timer Enable (WDT Disabled (SWDTEN Bit Controls))
#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window Size is 25%)

// DEVCFG0
#pragma config DEBUG = OFF              // Background Debugger Enable (Debugger is Disabled)
#pragma config JTAGEN = OFF             // JTAG Enable (JTAG Disabled)
#pragma config ICESEL = ICS_PGx1        // ICE/ICD Comm Channel Select (Communicate on PGEC1/PGED1)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <xc.h>
#include "pic32_uart.h"
#include "pic32_leds.h"
#include <plib.h>

#define FW_VER "2.1"
#define AUTHOR "EstebanPachon"

#define TRUE  1
#define FALSE 0

#define FREQUENCY (96000000)
#define PBCLK_FREQUENCY (96 * 1000 * 1000)

//CORE TIMER configuration
#define TOGGLES_PER_SEC 1000  
#define CORE_TICK_RATE (FREQUENCY / 2 / TOGGLES_PER_SEC)

//TIMER1 configuration
#define TICKS_PER_SEC 1000
#define T1_TICK_RATE (FREQUENCY / 8 / TICKS_PER_SEC)

//Change Notice Interrupt configuration
#define CONFIGURATION  (CND_ON| CND_IDLE_CON)
#define PINSWITCH    (CND6_ENABLE)
#define PULLUPS (CND_PULLUP_DISABLE_ALL)
#define INTERRUPT   (CHANGE_INT_PRI_2 | CHANGE_INT_ON)

//IO pins configuration
#define PINS_CREE BIT_2|BIT_3|BIT_10
#define PINS_LEDS BIT_4|BIT_6|BIT_7
#define PIN_SW BIT_6

//ADC variables
uint32_t ADC_channel14_value;
uint32_t ADC_offset;

//PWM RGB auxiliary variables
 uint8_t command_sent_from_console[4];
 uint16_t value_int;
 

//define setup parameters for openADC10
#define PARAM1 ADC_MODULE_ON | ADC_FORMAT_INTG | ADC_CLK_AUTO | ADC_AUTO_SAMPLING_ON
#define PARAM2 ADC_VREF_AVDD_AVSS | ADC_OFFSET_CAL_DISABLE | ADC_SCAN_OFF | ADC_SAMPLES_PER_INT_2 | ADC_ALT_BUF_ON | ADC_ALT_INPUT_ON
#define PARAM3 ADC_CONV_CLK_INTERNAL_RC | ADC_SAMPLE_TIME_12
#define PARAM4 SKIP_SCAN_ALL
#define PARAM5 ENABLE_AN14_ANA
//Configures the ADC MUX inputs
#define PARAM ADC_CH0_NEG_SAMPLEA_NVREF | ADC_CH0_POS_SAMPLEA_AN14

typedef volatile struct{
    uint16_t LED_pwm_on;
    uint16_t LED_pwm_period; 
    uint16_t macro_time_on;
    uint16_t macro_time_period;
    uint16_t adc_value;
}pwm_led_control_t;

typedef volatile struct{
    uint16_t LED_millis;
    uint16_t UART_millis;
    uint16_t LED1_millis; 
    uint16_t ADC_lecture_millis;
}variable_seconds_t;

typedef volatile struct{
    uint16_t led_counter;
    uint8_t button_push;
    uint16_t switch_millis_counter;
    uint8_t switch_state_LED_blinking;
    uint8_t flag_LED_stop_blinking:1;
}auxiliary_variables_t;

typedef volatile struct{
    uint16_t macro_period_rgb_time;
    uint16_t macro_on_rgb_time;
    uint16_t micro_period_rgb_time;
    uint16_t micro_on_rgb_time;
    uint8_t led_status:1;
    uint8_t pin;
    uint8_t port;
}pwm_led_control_rx_t;

typedef volatile struct{
    uint16_t UART_value_macro_period_time;
    uint16_t UART_value_macro_on_time;
    uint16_t UART_value_micro_period_time;
    uint16_t UART_value_micro_on_time;
}uart_values_t;

pwm_led_control_t pwm_led_configuration;
variable_seconds_t variable_seconds_configuration;
auxiliary_variables_t aux_variable_configuration;
pwm_led_control_rx_t led_red_rgb;
pwm_led_control_rx_t led_green_rgb;
pwm_led_control_rx_t led_blue_rgb;
uart_values_t uart_value_red_rgb;
uart_values_t uart_value_green_rgb;
uart_values_t uart_value_blue_rgb;

enum {
    e_invalid,
    e_get_adc_value,
    e_green_macro_on,
    e_green_macro_period,
    e_green_micro_on,
    e_green_micro_period,
    e_blue_macro_on,
    e_blue_macro_period,
    e_blue_micro_on,
    e_blue_micro_period,
    e_green_enable_blink,
    e_blue_enable_blink
};

void main_setup(void);
void init_configurations(void);
void load_initial_values(void);
void pwm_rgb_led_and_uart_initial_values(void);
void timers_initialiaze_configuration(void);
void adc_init_configuration(void);
void core_timer_counter_verification_values(void);
void timer1_counter_pwm_rgb_verification_values(void);
void pwm_rgb_red_led_verification_value();
void pwm_rgb_green_led_verification_value();
void pwm_rgb_blue_led_verification_value();
void start_uart_communication(void);
void toggle_LED1_each_second(void);
void toggle_LED2_each_second(void);
void send_UART_tick_message(void);
void toggle_LED_switch_pressed(void);
void stop_and_restart_LED_blinking(void);
void pwm_manual_potentiometer(void);
void sent_adc_conversion_uart(void);
void control_pwm_blue_rgb_via_uart(void);
void control_pwm_green_rgb_via_uart(void);
void uart_message_reception(void);
uint8_t uart_compare_to_select_command(uint8_t *command_sent_from_console);


